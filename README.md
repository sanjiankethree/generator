# generator

#### 项目介绍
代码生成工具

#### 软件架构
本项目采用spring-boot和freemarker<br/>
本项目无需使用数据库配置文件存储在<br/>
```
System.getProperty("java.io.tmpdir") +File.separator+ "project-generator/conn.s";
```

#### 安装教程

1. mvn clean package
2. java -jar generator.jar

#### 使用说明

注意字段create_time和update_time这两个字段是需要设置动态的，实际代码不需要管。<br/>
设置表属性：<br/>
    create_time 设置 CURRENT_TIMESTAMP属性<br/>
    update_time 设置 ON UPDATE CURRENT_TIMESTAMP属性<br/>
代码：
```
　CREATE TABLE `test_table` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=10000000 DEFAULT CHARSET=utf8mb4;

```

1. 项目主界面<br/>
<img src="img/1.png" alt="项目主界面" width="auto" height="400"/>
2. 配置数据源<br/>
<img src="img/2.png" alt="配置数据源" width="auto" height="400"/>
3. 选择生成的表格<br/>
<img src="img/3.png" alt="选择生成的表格" width="auto" height="400"/>
4. 配置po和dao层路径<br/>
<img src="img/4.jpg" alt="配置po和dao层路径" width="auto" height="400"/>
5. swagger2路径<br/>
url:http://localhost:8280/swagger-ui.html<br/>
<img src="img/5.png" alt="swagger2" width="auto" height="400"/>

## 关注

 http://www.jiagoujishu.com

![加入组织](https://images.gitee.com/uploads/images/2020/1019/140000_f873b872_1168339.jpeg "架构技术.jpg")

