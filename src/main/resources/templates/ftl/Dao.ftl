package ${daoPackageName}.${entityName}Dao;

import java.util.List;

import ${packageName}.${entityName}Po;

 /**
 * 
 * @Description: ${entityName}Dao
 * @author ${author}
 * @date ${time}
 *
 */
public interface  ${entityName}Dao {
	
	public List<${entityName}Po> select(${entityName}Po po);
	
	public ${entityName}Po selectById(String dbId);
	
	public int updateById(${entityName}Po po);
	
	public int insert(${entityName}Po record);
	
	public void updateList(List<${entityName}Po> list);

	public int update(${entityName}Po record);
	
	public ${entityName}Po selectOne(${entityName}Po record);
	
	public List<${entityName}Po> selectList(${entityName}Po record);
	
	public ${entityName}Po selectForUpdate(${entityName}Po record);
}