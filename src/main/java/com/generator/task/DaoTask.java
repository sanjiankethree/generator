package com.generator.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.generator.common.PropertyConfigUtils;
import com.generator.entity.ColumnEntity;
import com.generator.entity.DaoEntity;
import com.generator.entity.TableEntity;
import com.generator.framework.AbstractApplicationTask;
import com.generator.framework.ApplicationContext;
import com.generator.framework.Constants;
import com.generator.handler.BaseHandler;
import com.generator.handler.DaoHandler;
import com.generator.handler.EntityHandler;
import com.generator.handler.MapperHandle;
import com.generator.utils.StringUtil;
import com.generator.vo.ColumnInfo;
import com.generator.vo.EntityInfo;
import com.generator.vo.TableInfo;

/**
 * 
 * @Description: 实体生成类
 * @author Steven
 * @date 2017年9月27日 上午10:09:00
 *
 */
public class DaoTask extends AbstractApplicationTask
{
    private static final Logger logger = LoggerFactory.getLogger(DaoTask.class);
    
    private static String packageName = "";
    private static String daoPackageName = "";
    private static String DAO_FTL = "templates/ftl/Dao.ftl";
    
    @SuppressWarnings("unchecked")
    @Override
    protected boolean doInternal(ApplicationContext context) throws Exception
    {
        logger.info("实体类信息初始化");
        
        daoPackageName = (String) context.getAttribute("daoPackageName");
        daoPackageName = StringUtil.isEmpty(daoPackageName) ? "com.steven.demo" : daoPackageName;
        
        packageName = (String) context.getAttribute("packageName");
        packageName = StringUtil.isEmpty(packageName) ? "com.steven.demo" : packageName;
        String fileBasePath = (String) context.getAttribute("generator.path");

        Boolean result = combineInfo(context);
        if(result)
        {
            List<DaoEntity> daoEntities = (List<DaoEntity>) context.getAttribute("daoEntities");
            daoEntities.forEach(x -> {
            	BaseHandler<DaoEntity> handler = new DaoHandler(DAO_FTL, x, fileBasePath);
                handler.execute();
            });
            logger.info("Mapper类文件生成完毕");
        }
        return result;
    }
    
    /**
     * 
     * @Description: 组装信息
     * @author Steven
     * @date 2017年9月27日 上午10:19:33
     *
     */
    @SuppressWarnings("unchecked")
    private Boolean combineInfo(ApplicationContext context)
    {
        logger.info("Mapper类信息组装信息开始");
        
        Boolean result = true;
        
        try
        {
            // 获取表和实体的映射集合
            Map<String, String> table2Entities = (Map<String, String>) context.getAttribute("tableName.to.entityName");
            Map<String, TableInfo> tableInfos = (Map<String, TableInfo>) context.getAttribute("tableInfos");
            
            List<DaoEntity> daoEntities = new ArrayList<>();
            
            for(Entry<String, String> entry : table2Entities.entrySet())
            {
                String tableName = entry.getKey();
                String entityName = entry.getValue();
                DaoEntity daoEntity = new DaoEntity();
                TableInfo tableInfo = tableInfos.get(tableName);
                TableEntity tableEntity = new TableEntity();
                BeanUtils.copyProperties(tableEntity, tableInfo);
                
                List<ColumnInfo> columnInfos = tableInfo.getColumnList();
                List<ColumnEntity> columnEntities = new ArrayList<>();
                for(ColumnInfo columnInfo : columnInfos)
                {
                    ColumnEntity columnEntity = new ColumnEntity();
                    BeanUtils.copyProperties(columnEntity, columnInfo);
                    columnEntity.setPropName(StringUtil.convertFieldName2PropName(columnEntity.getName()));
                    columnEntity.setPropType(PropertyConfigUtils.getProperty(columnEntity.getType()));
                    columnEntities.add(columnEntity);
                }
                tableEntity.setColumns(columnEntities);
                daoEntity.setEntityName(entityName);
                daoEntity.setFileName(entityName + Constants.DAO_SUFFIX);
                daoEntity.setPackageName(packageName);
                daoEntity.setDaoPackageName(daoPackageName);
                daoEntity.setTableEntity(tableEntity);
                daoEntities.add(daoEntity);
            }
            context.setAttribute("daoEntities", daoEntities);
        }
        catch(Exception e)
        {
            result = false;
            e.printStackTrace();
        }
        return result;
    }
    
}
