package com.generator.handler;

import com.generator.entity.DaoEntity;

public class DaoHandler extends BaseHandler<DaoEntity>
{
    
    public DaoHandler(String ftlName, DaoEntity info, String fileBasePath)
    {
        this.ftlName = ftlName;
        this.info = info;
        this.savePath = fileBasePath + "dao/" + info.getFileName() + ".java";
    }
    
    @Override
    public void combileParams(DaoEntity info)
    {
        this.param.put("entityName", info.getEntityName());
        this.param.put("packageName", info.getPackageName());
        this.param.put("daoPackageName", info.getDaoPackageName());
        this.param.put("table", info.getTableEntity());
    }
    
}
